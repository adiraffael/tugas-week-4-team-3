const { query } = require("../models");

class Customer {
  async getAllCustomer(req, res, next) {
    try {
      const data = await query("SELECT * from customers order by id");
      if (data.length === 0) {
        return res.status(404).json({ errors: ["Costumers not found"] });
      }
      res.status(200).json({ data });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }
  async getDetailCustomer(req, res, next) {
    try {
      const data = await query("SELECT * from customers where id=?", [
        req.params.id,
      ]);
      if (data.length === 0) {
        return res.status(404).json({ errors: ["Costumers not found"] });
      }
      res.status(200).json({ data });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }

  async createCustomer(req, res, next) {
    try {
      const insertData = await query("INSERT INTO customers(name) VALUES(?)", [
        req.body.name,
      ]);

      const data = await query("SELECT * FROM customers", [
        insertData.insertId,
      ]);

      res.status(201).json({ data });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }

  async updateCustomer(req, res, next) {
    try {
      const updateData = await query("UPDATE customers SET name=? where id=?", [
        req.body.name,
        req.params.id,
      ]);

      if (updateData.affectedRows === 0) {
        return res.status(404).json({ errors: ["Customer not Found"] });
      }
      const data = await query("SELECT * FROM customers WHERE id=?", [
        req.params.id,
      ]);
      res.status(201).json({ data });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }

  async deleteCustomer(req, res, next) {
    try {
      const daleteData = await query("DELETE FROM customers where id=?", [
        req.params.id,
      ]);
      if (deleteData.affectedRows === 0) {
        return res.status(404).json({
          errors: ["Customer has been deleted or it's not exist"],
        });
      }
      res.status(200).json({ message: ["Success deleting data"] });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }
}

module.exports = new Customer();
