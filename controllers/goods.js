const { query } = require('../models');

class Good {

  async getAllGood(req, res, next) {
    try {
        const data = await query(
            'SELECT g.id, g.name as goodName, s.name as goodSupplier, g.price FROM goods g JOIN suppliers s ON g.id_supplier=s.id ORDER BY g.id',
            []
        );

        if (data.length === 0) {
            return res.status(404).json({ errors: ['Good not found'] });
        }

        res.status(200).json({ data });
        } catch (error) {
        res.status(500).json({ errors: ['Internal Server Error'] });
        }
    }

  async getDetailGood(req, res, next) {
    try {
        const data = await query(
            'SELECT g.id, g.name as goodName, s.name as goodSupplier, g.price FROM goods g JOIN suppliers s ON g.id_supplier=s.id WHERE g.id=?',
            [req.params.id]
        );

        if (data.length === 0) {
            return res.status(404).json({ errors: ['Good not found'] });
        }

        res.status(200).json({ data });
        } catch (error) {
        res.status(500).json({ errors: ['Internal Server Error'] });
        }
    }

  async createGood(req, res, next) {
    try {
        const insertedData = await query(
            'INSERT INTO goods(name, price, id_supplier) VALUES (?, ?, ?)',
            [req.body.name,
            req.body.price,
            req.body.id_supplier]
        );
    
        const data = await query(
            'SELECT g.id, g.name as goodName, s.name as goodSupplier, g.price FROM goods g JOIN suppliers s ON g.id_supplier=s.id ORDER BY g.id',
            [insertedData.insertId]
        );
    
      res.status(201).json({ data });
    } catch (error) {
      res.status(500).json({ errors: ['Internal Server Error'] });
    }
  }

  async updateGood(req, res, next) {
    try {
        const updatedData = await query(
            'UPDATE goods g SET name=?, price=?, id_supplier=? WHERE id=?',
            [req.body.name,
            req.body.price,
            req.body.id_supplier,
            req.params.id]
        );
    
        if (updatedData.affectedRows === 0) {
            return res.status(404).json({ errors: ['Good not found'] });
        }
    
        const data = await query(
            'SELECT g.id, g.name as goodName, s.name as goodSupplier, g.price FROM goods g JOIN suppliers s ON g.id_supplier=s.id WHERE g.id=?',
            [req.params.id]
        );
    
        res.status(200).json({ data });
        } catch (error) {
        res.status(500).json({ errors: ['Internal Server Error'] });
        }
    }

  async deleteGood(req, res, next) {
    try {
        const deletedData = await query('DELETE FROM goods WHERE id=?', [
            req.params.id,
        ]);

        if (deletedData.affectedRows === 0) {
            return res.status(404).json({ errors: ['Good not found or it is not exist'] });
        }

        res.status(200).json({ message: ['Success deleting data'] });
        } catch (error) {
        res.status(500).json({ errors: ['Internal Server Error'] });
        }
    }
}

module.exports = new Good();