const { query } = require("../models"); // import connection from models

class Suppliers {
  // get all suppliers
  async getAllSupplier(req, res, next) {
    try {
      // find all suppliers order by id suppliers
      const data = await query("SELECT * FROM suppliers ORDER BY id");

      // if data is empty array
      if (data.length === 0) {
        return res.status(404).json({ errors: ["Supplier not Found !"] });
      }

      res.status(200).json({ data });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }

  // get detail data
  async getDetailSupplier(req, res, next) {
    try {
      // find specific supplier by req.params.id
      const data = await query(
        "SELECT id, name as supplierName FROM suppliers WHERE id=?",
        [req.params.id]
      );

      // if data is empty array
      if (data.length === 0) {
        return res.status(404).json({ errors: ["Supplier not Found"] });
      }
      res.status(200).json({ data });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }

  // create supplier
  async createSupplier(req, res, next) {
    try {
      // insert data
      const insertedName = await query(
        "INSERT INTO suppliers(name) VALUES (?)",
        [req.body.name]
      );

      // find new data
      const data = await query("SELECT * FROM suppliers", [
        insertedName.insertId,
      ]);

      res.status(201).json({ data });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }

  async updateSupplier(req, res, next) {
    try {
      const updateSup = await query("UPDATE suppliers SET name=? WHERE id=?", [
        req.body.name,
        req.params.id,
      ]);

      if (updateSup.affectedRows === 0) {
        return res.status(404).json({
          errors: ["Suppliers not found"],
        });
      }

      // Find update supplier
      const data = await query("SELECT * FROM suppliers WHERE id=?", [
        req.params.id,
      ]);

      res.status(200).json({ data });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }
  async deleteSupplier(req, res, next) {
    try {
      const deleteData = await query("DELETE FROM suppliers WHERE id=?", [
        req.params.id,
      ]);

      if (deleteData.affectedRows === 0) {
        return res.status(404).json({
          errors: ["Suppliers has been deleted or it's not exist"],
        });
      }

      res.status(200).json({ data: [] });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }
}

module.exports = new Suppliers();
