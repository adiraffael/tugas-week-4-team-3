const express = require("express");

const {
  createSupplier,
  getAllSupplier,
  getDetailSupplier,
  updateSupplier,
  deleteSupplier,
} = require("../controllers/suppliers");

const router = express.Router();

router.get("/", getAllSupplier);
router.get("/:id", getDetailSupplier);
router.post("/", createSupplier);
router.put("/:id", updateSupplier);
router.delete("/:id", deleteSupplier);

module.exports = router;
