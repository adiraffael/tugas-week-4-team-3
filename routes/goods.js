const express = require('express');

const {
    createGood,
    getAllGood,
    getDetailGood,
    updateGood,
    deleteGood,
} = require('../controllers/goods');

const router = express.Router();

router.get('/', getAllGood);
router.get('/:id', getDetailGood);
router.post('/', createGood);
router.put('/:id', updateGood);
router.delete('/:id', deleteGood);

module.exports = router;