const express = require('express');

const {
  createCustomer,
  getAllCustomer,
  getDetailCustomer,
  updateCustomer,
  deleteCustomer,
} = require('../controllers/customers');

const router = express.Router();

router.get('/', getAllCustomer);
router.get('/:id', getDetailCustomer);
router.post('/', createCustomer);
router.put('/:id', updateCustomer);
router.delete('/:id', deleteCustomer);

module.exports = router;