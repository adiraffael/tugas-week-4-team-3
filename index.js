const express = require("express");

const customers = require('./routes/customers');
const goods = require("./routes/goods");
const suppliers = require("./routes/suppliers");

const port = process.env.PORT || 3000;

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use('/customers', customers);
app.use("/goods", goods);
app.use("/suppliers", suppliers);

app.listen(port, () => console.log(`Server running on ${port}`));
